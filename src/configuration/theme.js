export default {
    palette: {
        common: {
            white: '#fff',
            black: '#2C2C2C'
        },
        primary: {
            light: '#F5C16C',
            main: '#F09F19',
            dark: '#996610'
        },
        secondary: {
            main: '#2C2C2C'
        },
        background: {
            default: '#fff',
            sidebar: '#151b26'
        }
    },
}
