// react
import React from 'react';

// ext libs
import { useSnackbar } from 'notistack';
import { Redirect } from 'react-router-dom';

// mui
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

// app components
import InputWithButton from './components/InputWithButton';

// app services
// utils
// configuration
import { ACCESS_KEY } from '../../configuration/app';

// assets
// styles
const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%'
    },
});

const initialState = {
    isLoading: false,
    link: ''
};

function reducer(state, action) {
    switch (action.type) {
        case 'PROCESS_TRACKING':
            return {
                ...state,
                isLoading: true
            };
        case 'SHOW_TRACKING':
            return {
                ...state,
                link: action.link,
                isLoading: false
            };
        case 'RESET':
            return { ...state, ...initialState };
    }
}

export default function TrackShipment() {
    const classes = useStyles();
    const { enqueueSnackbar } = useSnackbar();

    const [ state, dispatch ] = React.useReducer(reducer, initialState);
    const [ wayBillNumber, setWayBillNumber ] = React.useState('');

    function onChangeWayBillNumber(event) {
        const { value } = event.target;
        setWayBillNumber(value.trim());
    }

    function onSubmit(event) {
        event.preventDefault();

        if (!wayBillNumber) {
            enqueueSnackbar('Please enter a way bill number');
            return false;
        }

        function _onError(e) {
            console.error('[TrackShipment::onSubmit] error - ', e);
            enqueueSnackbar('Oops! We are unable to process the way bill number. Please try again');
        }

        function _onSuccess(response) {
            return response.json()
        }

        function _handleSuccess(data) {
            const { status, link } = data;
            if (status === 'success') {
                dispatch({ type: 'SHOW_TRACKING', link });
            } else {
                enqueueSnackbar('Please check your way bill number and try again');
            }
        }

        dispatch({ type: 'PROCESS_TRACKING' });

        fetch(`https://www.freighttiger.com/saas/trip/share?accessKey=${ACCESS_KEY}&trip_id=${wayBillNumber}`)
            .then(_onSuccess)
            .then(_handleSuccess)
            .catch(_onError);
    }

    return (
        <React.Fragment>
            {state.link && <Redirect to={{ pathname: '/tracking', state: { link: state.link } }} />}
            <div className={classes.root}>
                <Typography variant='h3'>Track Shipment</Typography>
                <form className={classes.form} onSubmit={onSubmit}>
                    <InputWithButton
                        wayBillNumber={wayBillNumber}
                        onChangeWayBillNumber={onChangeWayBillNumber}
                    />
                    <Typography variant='caption' display='block' gutterBottom>
                        Try with: AWB22348906
                    </Typography>
                </form>
            </div>
        </React.Fragment>
    );
}
