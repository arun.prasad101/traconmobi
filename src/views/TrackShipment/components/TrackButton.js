// react
import React from 'react';

// ext libs
// mui
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// app components
// app services
// utils
// configuration
// assets
// styles

const StyledButton = withStyles(theme => ({
    root: {
        display: 'flex',
        flex: 1,
        color: theme.palette.common.white,
        borderRadius: 0,
        height: '100%',
        maxWidth: 200,
        fontWeight: 'bold'
    },
}))(Button);

export default function TrackButton() {
    return (
        <StyledButton
            color='primary'
            disableElevation={true}
            size='large'
            type='submit'
            variant='contained'
        >
            Track
        </StyledButton>
    );
}
