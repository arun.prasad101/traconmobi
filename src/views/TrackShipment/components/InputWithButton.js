// react
import React from 'react';
import PropTypes from 'prop-types';

// ext libs
// mui
import { makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

// app components
import TrackButton from './TrackButton';

// app services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles({
    formControl: {
        margin: '24px 0',
        display: 'flex',
        flex: 1,
        maxWidth: 700,
        height: 72,
        background: '#fff 0% 0% no-repeat padding-box',
        boxShadow: '0px 6px 15px #00000029',
        borderRadius: 4,
        opacity: 1,
    },
    input: {
        display: 'flex',
        flex: 2,
        padding: 20
    },
});

export default function InputWithButton({ wayBillNumber, onChangeWayBillNumber }) {
    const classes = useStyles();

    return (
        <div className={classes.formControl}>
            <InputBase
                className={classes.input}
                placeholder='Enter Waybill'
                value={wayBillNumber}
                onChange={onChangeWayBillNumber}
            />
            <TrackButton />
        </div>
    );
}

InputWithButton.propTypes = {
    wayBillNumber: PropTypes.string,
    onChangeWayBillNumber: PropTypes.func
};
