// react
import React from 'react';

// ext libs
// mui
import { makeStyles } from '@material-ui/core/styles';

// app components
// app services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles({
    root: {
        display: 'flex',
        flex: 1,
        height: '100%'
    },
    trackingContainer: {
        flex: 1
    }
});

export default function TrackingView({ location }) {
    const classes = useStyles();

    React.useEffect(() => {
        console.log(location.state.link);
    }, []);

    return (
        <div className={classes.root}>
            <iframe
                className={classes.trackingContainer}
                frameBorder='0'
                height='100%'
                width='100%'
                src={location.state.link}
            />
        </div>
    );
}
