// react
import React from 'react';

// ext libs
import { BrowserRouter, Switch } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';

// mui
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

// app components
import { AppRoute } from './components/Router';

// app views
import TrackShipment from './views/TrackShipment';
import TrackingView from './views/TrackingView';

// app services
// utils
// configuration
// assets
// styles
import theme from './configuration/theme';


// create the MUI theme
const muiTheme = createMuiTheme(theme);

function App() {
    return (
        <SnackbarProvider maxSnack={3}>
            <MuiThemeProvider theme={muiTheme}>
                <CssBaseline />
                <BrowserRouter>
                    <Switch>
                        <AppRoute path='/' exact component={TrackShipment} />
                        <AppRoute path='/tracking' component={TrackingView} noBGImage />
                    </Switch>
                </BrowserRouter>
            </MuiThemeProvider>
        </SnackbarProvider>
    );
}

export default App;
