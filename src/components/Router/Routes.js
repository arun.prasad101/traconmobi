// react
import React from 'react';
import PropTypes from 'prop-types';

// ext libs
import { Route } from 'react-router-dom';

// mui
// app components
import AppLayout from '../AppLayout'

// app services
// utils
// configuration
// assets
// styles

export function AppRoute({ component: Component, noBGImage, noPadding, ...rest }) {
    return <Route
        {...rest}
        render={ props => (
            <AppLayout noPadding={noPadding} noBGImage={noBGImage}>
                <Component {...props} />
            </AppLayout>
        )}
    />
}

AppRoute.propTypes = {
    ...Route.propTypes,
    /**
     * Remove the left & right padding
     */
    noPadding: PropTypes.bool,
    /**
     * Remove the background image
     */
    noBGImage: PropTypes.bool
};

AppRoute.defaultProps = {
    noPadding: false,
    noBGImage: false
};
