// react
import React from 'react';

// ext libs
import { Link } from 'react-router-dom';

// mui
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

// app components
import { makeStyles } from '@material-ui/core/styles';

// app services
// utils
// configuration
// assets
import logo from '../../assets/logo.png';

// styles
const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: 'transparent',
        boxShadow: 'none'
    },
    toolbar: {
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 10,
            paddingRight: 10
        },
        [theme.breakpoints.up('sm')]: {
            paddingLeft: 204,
            paddingRight: 204
        },
        paddingTop: 26,
        boxShadow: 'none',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    logo: {
        height: 50
    },
    headerRight: {
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },
    headerLink: {
        color: '#6E6E6E',
        textDecoration: 'none'
    }
}));

export default function AppHeader() {
    const classes = useStyles();

    return (
        <AppBar position='static' className={classes.root}>
            <Toolbar className={classes.toolbar}>
                <div>
                    <Link to='/'>
                        <img src={logo} alt='TracOnMobi' className={classes.logo} />
                    </Link>
                </div>
                <div className={classes.headerRight}>
                    <a href='#' className={classes.headerLink}>About us</a>
                </div>
            </Toolbar>
        </AppBar>
    );
}
