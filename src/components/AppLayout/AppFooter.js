// react
import React from 'react';

// ext libs
// mui
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

// app components
// app services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles(theme => ({
    root: {
        padding: '0 204px',
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        color: '#6E6E6E',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    }
}));

export default function AppFooter() {
    const classes = useStyles();

    return (
        <footer className={classes.root}>
            <Typography style={{ paddingRight: 58 }}>@ TraconMobi Solutions Pvt. Ltd.</Typography>
            <Typography>Terms of use</Typography>
        </footer>
    );
}
