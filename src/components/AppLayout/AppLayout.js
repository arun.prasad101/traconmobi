// react
import React from 'react';

// ext libs
import classNames from 'classnames';

// mui
import { makeStyles } from '@material-ui/core/styles';

// app components
import AppHeader from './AppHeader';
import AppFooter from './AppFooter';
import backgroundImage from "../../assets/background.png";

// app services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles(theme => ({
    appRoot: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        background: `url(${backgroundImage}) no-repeat center center fixed`,
        backgroundSize: 'cover',
    },
    pageContentRoot: {
        [theme.breakpoints.down('sm')]: {
            padding: '20px 10px'
        },
        padding: '20px 204px',
        flexGrow: 1
    },
    noPadding: {
        padding: '20px'
    },
    noBGImage: {
        background: '#fff'
    }
}));

export default function AppLayout({ children, noPadding, noBGImage }) {
    const classes = useStyles();

    const appRootStyles = classNames({
        [classes.appRoot]: true,
        [classes.noBGImage]: noBGImage
    });

    const contentRootStyles = classNames({
        [classes.pageContentRoot]: true,
        [classes.noPadding]: noPadding,
    });

    return (
        <main className={appRootStyles}>
            <AppHeader />
            <section className={contentRootStyles}>
                {children}
            </section>
            <AppFooter />
        </main>
    );
}
